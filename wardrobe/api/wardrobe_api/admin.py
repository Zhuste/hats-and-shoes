from django.contrib import admin

from .models import Location, Bin

@admin.register(Location)
class LocationAdmin(admin.ModelAdmin):
    pass

admin.site.register(Bin)
class Bin(admin.ModelAdmin):
    list_display = (
        "closet_name",
        "bin_number",
        "bin_size",
    )
