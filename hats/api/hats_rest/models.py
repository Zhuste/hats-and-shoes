from django.db import models
from django.urls import reverse


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200, null=True, unique=True)
    closet_name = models.CharField(max_length=200)
    section_number = models.PositiveSmallIntegerField(null=True)
    shelf_number = models.PositiveSmallIntegerField(null=True)

    def __str__(self):
        return self.closet_name

class Hat(models.Model):
    style_name = models.CharField(max_length=150)
    fabric = models.CharField(max_length=150)
    color = models.CharField(max_length=150)
    url = models.URLField(null=True)

    location = models.ForeignKey(
        LocationVO,
        related_name="hats",
        on_delete=models.CASCADE,
    )

    def __str__(self):
        return self.style_name

    def get_api_url(self):
        return reverse("api_show_hat", kwargs={"id": self.id})
