from django.urls import path
from .views import api_shoes, delete_api_shoe


urlpatterns = [
    # path("shoes/", api_shoes, name="api_create_shoe"),
    path("shoes/", api_shoes, name="api_create_shoe"),
    path("bins/<int:bin_vo_id>/shoes/",api_shoes,name="api_list_shoes"),
    path("shoes/<int:pk>/", delete_api_shoe, name="delete_api_shoe"),
    # path("bins/<int:bin_vo_id>/shoes/",api_shoes,name="api_list_shoes"),
]
