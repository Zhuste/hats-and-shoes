import json
from django.http import JsonResponse
from django.shortcuts import render, redirect
from .models import Shoe, BinVO
from django.views.decorators.http import require_http_methods
from common.json import DateEncoder, QuerySetEncoder, ModelEncoder
# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ["manufacturer, model", "color", "picture"]
    def get_extra_data(self, o):
        return {"shoe_bin": o.shoe_bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture",
        "shoe_bin",
    ]
    encoders = {
        "shoe_bin": BinVODetailEncoder(),
    }

    # def get_extra_data(self, o):
    #     count = BinVO.objects.filter(email=o.email).count()
    #     return {"has_account": count > 0}

#This handles shoe LIST!! (showing list and adding a new object to list)
@require_http_methods(["GET", "POST"])
    #list shoes for a get request!
def api_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(shoe_bin=bin_vo_id)
            shoe_list = []
            for shoe in shoes:
                shoe_list.append({
                'manufacturer': shoe.manufacturer,
                'model': shoe.model,
                'color': shoe.color,
                'picture': shoe.picture,
                'shoe_bin': shoe.shoe_bin.closet_name,
                'id': shoe.id,
                })
            return JsonResponse({'shoes':shoe_list})
        else:
            shoes = Shoe.objects.all()
            shoe_list = []
            for shoe in shoes:
                shoe_list.append({
                'manufacturer': shoe.manufacturer,
                'model': shoe.model,
                'color': shoe.color,
                'picture': shoe.picture,
                'shoe_bin': shoe.shoe_bin.closet_name,
                'id': shoe.id,
                })
            return JsonResponse({'shoes':shoe_list},
                                encoder=ShoeListEncoder,)
    #create a new shoe !!! Not using encoder, using silly dictionary instead.
    else:
        content = json.loads(request.body)

        try:
            shoe_bin_href = content["shoe_bin"]
            shoe_bin = BinVO.objects.get(import_href=shoe_bin_href)
            content["shoe_bin"] = shoe_bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )

        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

#This function specifically handles a certain Shoe (delete shoe, show shoe detail, edit shoe!)

def delete_api_shoe(request, pk):
    shoe = Shoe.objects.filter(id=pk).first()
    shoe.delete()
    return JsonResponse(
        {'shoe':shoe},
        encoder=ShoeDetailEncoder,
        safe=False,
    )
