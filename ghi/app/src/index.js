import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

async function LoadHatsAndShoes() {
  const shoeResponse = await fetch('http://localhost:8080/api/shoes/');
  const hatResponse = await fetch('http://localhost:8090/api/hats/');

  if (hatResponse.ok && shoeResponse.ok) {
    const hatData = await hatResponse.json();
    const shoeData = await shoeResponse.json();
    root.render(
      <React.StrictMode>
        <App shoes={shoeData.shoes} hats={hatData} />
      </React.StrictMode>
    );
  } else {
    console.error(hatResponse || shoeResponse);
  }
}

LoadHatsAndShoes();
