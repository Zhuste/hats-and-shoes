import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ShoeList from './ShoeList';
import ShoeForm from './ShoeForm';
import Nav from './Nav';
import HatForm from './HatForm';
import HatListing from './HatListing';


function App(props) {
  if (props.shoes === undefined && props.hats === undefined) {
    return null;
  }

  // function App(props) {
  //   if (props.shoes === undefined) {
  //     return null;
  // }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/shoes" element={<ShoeList shoes={props.shoes} />} />
          <Route path="/shoes/new" element={<ShoeForm shoes={props.shoes} />} />
          <Route path="hats">
            <Route path="" element={<HatListing hats={props.hats} />} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
