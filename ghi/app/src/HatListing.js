import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function HatColumn(props) {
    return (
        <div className="col">
            {props.list.map(data => {
                const hat = data;
                return (
                    <div key={hat.href} className="card mb-3 shadow">
                            <img src={hat.url} className="card-img-top" />

                        <div className="card-body">
                            <h5 className="card-title">{hat.style_name}</h5>
                            {/* <h6 className="card-subtitle mb-2 text-muted">
                                {hat.location.section_number}
                            </h6> */}
                            <h6 className="card-subtitle mb-2 text-muted">
                                fabric: {hat.fabric}
                            </h6>
                            <h6 className="card-subtitle mb-2 text-muted">
                                color: {hat.color}
                            </h6>
                        </div>
                        <div className="card-footer d-flex align-items-center justify-content-center">
                                <button onClick={() => deleteButton(hat.href)} className="btn btn-outline-danger" data-toggle="button" aria-pressed="false" type="button">
                                    Remove from Selection
                                </button>
                        </div>
                    </div>
                );
            })}
        </div>
    );
    }


const deleteButton = async (href) => {
    const confirm = window.confirm("Deleting hat, are you sure?");
    if (confirm) {
        fetch(`http://localhost:8090${href}`, {
            method: 'DELETE'
        })
        window.location.reload();
}}

const HatListing = (props) =>  {
    const [hatColumns, setHatColumns] = useState([[], [], []]);

    const fetchData = async () => {
        const url = 'http://localhost:8090/api/hats/';

        try {
        const response = await fetch(url);
        if (response.ok) {
            // Get the list of hats
            const data = await response.json();

            // Create a list of for all the requests and
            // add all of the requests to it
            const requests = [];
            for (let hat of data.hats) {
            const detailUrl = `http://localhost:8090${hat.href}`;
            requests.push(fetch(detailUrl));
            }

            const responses = await Promise.all(requests);

            const columns = [[], [], []];

            let i = 0;
            for (const hatResponse of responses) {
            if (hatResponse.ok) {
                const details = await hatResponse.json();
                columns[i].push(details);
                i = i + 1;
                if (i > 2) {
                i = 0;
                }
            } else {
                console.error(hatResponse);
            }
            }

            setHatColumns(columns);
        }
        } catch (e) {
        console.error(e);
        }
    }

useEffect(() => {
    fetchData();
}, []);

return (
    <>
    <div className="px-4 py-5 my-5 mt-0 text-center">
        <img className="bg-white rounded shadow d-block mx-auto mb-4" src="https://tinyurl.com/Hat-couple" alt="" width="800" />
        <h1 className="display-5 fw-bold">All the Hats you could ever Want!</h1>
        <div className="col-lg-6 mx-auto">
        <p className="lead mb-4">
            The only selection of Hats you'll ever Need!
        </p>
        <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
            <Link to="/hats/new" className="btn btn-primary btn-lg px-4 gap-3">Add a Hat</Link>
        </div>
        </div>
    </div>
    <div className="container text-center">
        <h2>Hat Selection</h2>
        <div className="row">
        {hatColumns.map((hatList, index) => {
            return (
            <HatColumn key={index} list={hatList} />
            );
        })}
        </div>
    </div>
    </>
);
}

export default HatListing;
